//Author: Ben McIlmoyle

#include <iostream>

#include "gtest/gtest.h"
#include "CVector3f.h"
#include "mathFuncsDll.h"

////////////////
// Whitebox 1 //
////////////////

// Test the normalization of the vector3 class with random input and then check against manually calculated values
TEST(Normalization, RandomPositiveVector)
{
	CVector3f vec3Test(1.0f, 5.0f, 3.0f);

	//Run the normalize call
	vec3Test.Normalize();

	//Check the 3 vec3 values
	ASSERT_FLOAT_EQ(0.169030845f, vec3Test.x);
	ASSERT_FLOAT_EQ(0.845154226f, vec3Test.y);
	ASSERT_FLOAT_EQ(0.507092535f, vec3Test.z);
}

////////////////
// Whitebox 2 //
////////////////

// Test the magnitude function of the vector3 class with 3 postivies values and then check against manually calculated values
TEST(Magnitude, RandomPositiveVector)
{
	CVector3f vec3Test(10.0, 12.0f, 13.0f);

	//Check the magnitude
	ASSERT_FLOAT_EQ(20.3224010f, vec3Test.Magnitude());
}

////////////////
// Whitebox 3 //
////////////////

// Test the reverse function of the vector3 class with 3 postivies values and then check against manually calculated values (just inversed numbers)
TEST(Reverse, RandomPositiveVector)
{
	CVector3f vec3Test(123.0f, 456.7f, 786423.456f);

	vec3Test.Reverse();

	//Check the 3 vec3 values
	ASSERT_FLOAT_EQ(-123.0f, vec3Test.x);
	ASSERT_FLOAT_EQ(-456.7f, vec3Test.y);
	ASSERT_FLOAT_EQ(-786423.456f, vec3Test.z);
}

////////////////
// Whitebox 4 //
////////////////

// Test the reverse function of the vector3 class with a positive Y value and then check against if with manually calculated value
TEST(ScalarMultiply, UpVec)
{
	CVector3f vec3Test(0.0f, 1.0f, 0.0f);

	vec3Test = vec3Test.ScalarMultiply(vec3Test, 2.0f);

	//Check the 3 vec3 values
	ASSERT_FLOAT_EQ(0.0f, vec3Test.x);
	ASSERT_FLOAT_EQ(2.0f, vec3Test.y);
	ASSERT_FLOAT_EQ(0.0f, vec3Test.z);
}

////////////////
// Whitebox 5 //
////////////////

// Test the Distance function of the vector3 class with a zero vector and a  positive Y vector. Should return distance between vectors
TEST(Distance, ZeroVecUpVec)
{
	CVector3f vec3Test(0.0f, 11.5f, 0.0f);
	CVector3f vec3Test2(0.0f, 0.0f, 0.0f);

	//Check the distance
	ASSERT_FLOAT_EQ(11.5f, vec3Test.Distance(vec3Test, vec3Test2));
}

////////////////
// Whitebox 6 //
////////////////

// Test the DistanceSquared function of the vector3 class with a zero vector and a  positive Y vector. Should return distance between vectors squared (distance * distance)
TEST(DistanceSquared, ZeroVecUpVec)
{
	CVector3f vec3Test(0.0f, 2.0f, 0.0f);
	CVector3f vec3Test2(0.0f, 0.0f, 0.0f);

	//Check the distance
	ASSERT_FLOAT_EQ(4.0f, vec3Test.DistanceSquared(vec3Test, vec3Test2));
}

////////////////
// Whitebox 7 //
////////////////

// Test the ScalarDivide function of the vector3 class with a zero vector and a positive scalar value. Should return all zero's since you can't divide by 0
TEST(ScalarDivide, ZeroVecPositiveScalar)
{
	CVector3f vec3Test(0.0f, 0.0f, 0.0f);

	vec3Test = vec3Test.ScalarDivide(vec3Test, 2.f);

	//Check the 3 vec3 values
	ASSERT_FLOAT_EQ(0.0f, vec3Test.x);
	ASSERT_FLOAT_EQ(0.0f, vec3Test.y);
	ASSERT_FLOAT_EQ(0.0f, vec3Test.z);
}

////////////////
// Whitebox  8//
////////////////

// Test the Subtract function of the vector3 class with two Y positive vectors. Should return the vectorA - VectorB
TEST(Subtract, ZeroVecPositiveScalar)
{
	CVector3f vec3Test(0.0f, 10.0f, 0.0f);
	CVector3f vec3Test2(0.0f, 5.0f, 0.0f);

	vec3Test = vec3Test.Subtract(vec3Test, vec3Test2);

	//Check the 3 vec3 values
	ASSERT_FLOAT_EQ(0.0f, vec3Test.x);
	ASSERT_FLOAT_EQ(5.0f, vec3Test.y);
	ASSERT_FLOAT_EQ(0.0f, vec3Test.z);
}

////////////////
// Blackbox  1//
////////////////

// Test the add function of the MathFuncsDll by checking a + b
TEST(Blackbox, addDoubles)
{
	MathFuncs::MyMathFuncs MathFuncsDll;
	EXPECT_DOUBLE_EQ(4.0, MathFuncsDll.Add(2.0,2.0));
}

////////////////
// Blackbox  2//
////////////////

// Test the square function of the MathFuncsDll by checking a * a
TEST(Blackbox, squareDouble)
{
	MathFuncs::MyMathFuncs MathFuncsDll;
	EXPECT_DOUBLE_EQ(4.0, MathFuncsDll.Square(2.0));
}

int main(int argc, char **argv)
{
	//Init google testing
	::testing::InitGoogleTest(&argc, argv);

	//Run the test that are defined
	RUN_ALL_TESTS();

	return 0;
}