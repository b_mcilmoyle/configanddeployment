To compile please use the VS developer command prompt and enter the following command

MSbuild.exe "YourLocalDirectory"\XboxControllerOpenGLDemo.vcxproj /t:project_04 /property:Configuration=Release

The project is of an Xbox 360 and Xbox One controller demo I created during reading week. If you really want to see it do anything you'll need an Xbox controller. The project should still run fine without a controller but you won't be able to see the interactions with the virtual controller displayed obviously. 
