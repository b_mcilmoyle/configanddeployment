#ifndef __XML_TOOL_H__
#define __XML_TOOL_H__

#include "cocos2d.h"
#include "rapidxml_utils.hpp"

class XMLTool
{
public:

	static void loadSceneFromXML(cocos2d::Layer* Layer, std::string xmlFileName);
	static void saveSceneToXML(cocos2d::Layer* Layer, std::string xmlFileName);

private:

	static void createSprite(cocos2d::Layer* Layer, rapidxml::xml_node<>* spriteNode);
	static void createPlayerSprite(cocos2d::Layer* Layer, rapidxml::xml_node<>* spriteNode);
	static void createLabel(cocos2d::Layer* Layer, rapidxml::xml_node<>* labelNode);
};

#endif // __XML_TOOL_H__