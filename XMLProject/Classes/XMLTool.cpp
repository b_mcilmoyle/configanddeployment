#include "XMLTool.h"
#include "rapidxml_print.hpp"

void XMLTool::loadSceneFromXML(cocos2d::Layer* Layer, std::string xmlFileName)
{
	//Open the XML file
	rapidxml::file<> xmlFile(xmlFileName.c_str()); // Default template is char
	rapidxml::xml_document<> doc;
	doc.parse<0>(xmlFile.data());

	//Set the root node
	rapidxml::xml_node<>* root = doc.first_node("scene");

	//Set the first child "sprite" node
	rapidxml::xml_node<>* spriteNode = root->first_node("sprite");

	//Loop through each model node
	while (spriteNode != NULL)
	{
		//Process the current nide
		createSprite(Layer, spriteNode);

		//Find the next sprite node
		spriteNode = spriteNode->next_sibling("sprite");
	}

	//Create the player sprite
	//Set the first child "sprite" node
	rapidxml::xml_node<>* playerNode = root->first_node("playerSprite");
	createPlayerSprite(Layer, playerNode);

	//Add labels
	rapidxml::xml_node<>* sceneConfig = root->first_node("sceneConfig");
	rapidxml::xml_node<>* labelNode = sceneConfig->first_node("label");

	//Loop through each label node
	while (labelNode != NULL)
	{
		//Process the current nide
		createLabel(Layer, labelNode);

		//Find the next sprite node
		labelNode = labelNode->next_sibling("label");
	}
}

void XMLTool::createSprite(cocos2d::Layer* Layer, rapidxml::xml_node<>* spriteNode)
{
	//ID
	std::string id = spriteNode->first_node("id")->value();

	//Image file
	std::string image = spriteNode->first_node("image")->value();
	auto sprite = cocos2d::Sprite::create(image);
	sprite->setName(id);

	//Visable
	std::string visable = spriteNode->first_node("visible")->value();
	if (visable == "f")
		sprite->setVisible(false);

	//Z layer (depth on z axis)
	int zDepth = atoi(spriteNode->first_node("zDepth")->value());

	//Anchor point (rotation point)
	rapidxml::xml_node<>* anchorNode = spriteNode->first_node("anchor");
	float anchorX = atof(anchorNode->first_node("x")->value());
	float anchorY = atof(anchorNode->first_node("y")->value());
	sprite->setAnchorPoint(cocos2d::Vec2(anchorX, anchorY));

	//Position
	rapidxml::xml_node<>* positionNode = spriteNode->first_node("position");
	float postionX = atof(positionNode->first_node("x")->value());
	float postionY = atof(positionNode->first_node("y")->value());
	sprite->setPosition(cocos2d::Vec2(postionX, postionY));

	//Rotation
	rapidxml::xml_node<>* rotationNode = spriteNode->first_node("rotation");
	sprite->setRotation(atof(rotationNode->value()));

	//Scale

	rapidxml::xml_node<>* scaleNode = spriteNode->first_node("scale");
	float scaleX = atof(scaleNode->first_node("x")->value());
	float scaleY = atof(scaleNode->first_node("y")->value());
	sprite->setScaleX(scaleX);
	sprite->setScaleY(scaleY);

	//Skew
	rapidxml::xml_node<>* skewNode = spriteNode->first_node("skew");
	float skewX = atof(skewNode->first_node("x")->value());
	float skewY = atof(skewNode->first_node("y")->value());
	sprite->setSkewX(skewX);
	sprite->setSkewY(skewY);

	//Flipped
	rapidxml::xml_node<>* flippedNode = spriteNode->first_node("flipped");
	std::string flippedX = flippedNode->first_node("x")->value();
	std::string flippedY = flippedNode->first_node("y")->value();

	if (flippedX == "t")
		sprite->setFlippedX(true);

	if (flippedY == "t")
		sprite->setFlippedY(true);

	// add the sprite as a child to this layer
	Layer->addChild(sprite, zDepth);
}

void XMLTool::createPlayerSprite(cocos2d::Layer* Layer, rapidxml::xml_node<>* spriteNode)
{
	//ID
	std::string id = spriteNode->first_node("id")->value();

	//Image file
	std::string image = spriteNode->first_node("image")->value();
	auto sprite = cocos2d::Sprite::create(image);
	sprite->setName(id);

	//Visable
	std::string visable = spriteNode->first_node("visible")->value();
	if (visable == "f")
		sprite->setVisible(false);

	//Z layer (depth on z axis)
	int zDepth = atoi(spriteNode->first_node("zDepth")->value());

	//Anchor point (rotation point)
	rapidxml::xml_node<>* anchorNode = spriteNode->first_node("anchor");
	float anchorX = atof(anchorNode->first_node("x")->value());
	float anchorY = atof(anchorNode->first_node("y")->value());
	sprite->setAnchorPoint(cocos2d::Vec2(anchorX, anchorY));

	//Position
	rapidxml::xml_node<>* positionNode = spriteNode->first_node("position");
	float postionX = atof(positionNode->first_node("x")->value());
	float postionY = atof(positionNode->first_node("y")->value());
	sprite->setPosition(cocos2d::Vec2(postionX, postionY));

	//Rotation
	rapidxml::xml_node<>* rotationNode = spriteNode->first_node("rotation");
	sprite->setRotation(atof(rotationNode->value()));

	//Scale
	rapidxml::xml_node<>* scaleNode = spriteNode->first_node("scale");
	float scaleX = atof(scaleNode->first_node("x")->value());
	float scaleY = atof(scaleNode->first_node("y")->value());
	sprite->setScaleX(scaleX);
	sprite->setScaleY(scaleY);

	//Skew
	rapidxml::xml_node<>* skewNode = spriteNode->first_node("skew");
	float skewX = atof(skewNode->first_node("x")->value());
	float skewY = atof(skewNode->first_node("y")->value());
	sprite->setSkewX(skewX);
	sprite->setSkewY(skewY);

	//Flipped
	rapidxml::xml_node<>* flippedNode = spriteNode->first_node("flipped");
	std::string flippedX = flippedNode->first_node("x")->value();
	std::string flippedY = flippedNode->first_node("y")->value();

	if (flippedX == "t")
		sprite->setFlippedX(true);

	if (flippedY == "t")
		sprite->setFlippedY(true);

	// add the sprite as a child to this layer
	Layer->addChild(sprite, zDepth);
}

void XMLTool::createLabel(cocos2d::Layer* Layer, rapidxml::xml_node<>* labelNode)
{
	//Text
	std::string labelText = labelNode->first_node("text")->value();

	//Font
	std::string labelFont = labelNode->first_node("font")->value();

	//fontsize
	int fontSize = atoi(labelNode->first_node("fontSize")->value());

	//position
	rapidxml::xml_node<>* positionNode = labelNode->first_node("position");
	float postionX = atof(positionNode->first_node("x")->value());
	float postionY = atof(positionNode->first_node("y")->value());

	//Z layer (depth on z axis)
	int zDepth = atoi(labelNode->first_node("zDepth")->value());

	cocos2d::Label* label = cocos2d::Label::createWithSystemFont(labelText, labelFont, fontSize);
	label->setPosition(postionX, postionY);
	Layer->addChild(label, zDepth);
}

void XMLTool::saveSceneToXML(cocos2d::Layer* Layer, std::string xmlFileName)
{
	//Open the XML file
	rapidxml::file<> xmlFile(xmlFileName.c_str()); // Default template is char
	rapidxml::xml_document<> doc;
	doc.parse<rapidxml::parse_no_data_nodes>(xmlFile.data());
	
	//Set the root node
	rapidxml::xml_node<>* root = doc.first_node("scene");

	//Set the first child "sprite" node
	rapidxml::xml_node<>* spriteNode = root->first_node("playerSprite");

	//Get the player
	cocos2d::Sprite* player = dynamic_cast<cocos2d::Sprite*>(Layer->getChildByName("mario"));

	//Use a string stream to convert the position
	std::ostringstream ss;

	ss << player->getPositionX();
	std::string positionX(ss.str());


	std::ostringstream ss2;
	ss2 << player->getPositionY();
	std::string positionY(ss2.str());

	//Set Position
	rapidxml::xml_node<>* positionNode = spriteNode->first_node("position");
	positionNode->first_node("x")->value(positionX.c_str());
	positionNode->first_node("y")->value(positionY.c_str());

	//Set flip
	rapidxml::xml_node<>* flippedNode = spriteNode->first_node("flipped");

	if (player->isFlipX())
		flippedNode->first_node("x")->value("t");
	else
		flippedNode->first_node("x")->value("f");

	//Write to wile
	std::ofstream myfile;
	myfile.open(xmlFileName);
	myfile << doc;
}