#include "SceneOne.h"
#include "SceneTwo.h"
#include "SceneThree.h"
#include "XMLTool.h"

USING_NS_CC;

Scene* SceneOne::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
	auto layer = SceneOne::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool SceneOne::init()
{
    //////////////////////////////
    // 1. super init first
	if (!LayerColor::initWithColor(Color4B(80, 174, 229, 255)))
    {
        return false;
    }

	//Setup keyboard
	auto keyboardListener = EventListenerKeyboard::create();
	
	keyboardListener->onKeyPressed = [=](EventKeyboard::KeyCode keyCode, Event* event)
	{
		// If a key already exists, do nothing as it will already have a time stamp
		// Otherwise, set's the timestamp to now
		if (keys.find(keyCode) == keys.end())
		{
			keys[keyCode] = std::chrono::high_resolution_clock::now();
		}

		//Save when 's' is pressed
		if (keyCode == EventKeyboard::KeyCode::KEY_S)
			XMLTool::saveSceneToXML(this, "../Resources/scene1.xml");

		//Load scene one
		if (keyCode == EventKeyboard::KeyCode::KEY_1)
		{
			auto scene = SceneOne::createScene();
			Director::getInstance()->replaceScene(TransitionFade::create(0.1, scene));
		}

		//Load scene two
		if (keyCode == EventKeyboard::KeyCode::KEY_2)
		{
			auto scene = SceneTwo::createScene();
			Director::getInstance()->replaceScene(TransitionFade::create(0.1, scene));
		}

		//Load scene three
		if (keyCode == EventKeyboard::KeyCode::KEY_3)
		{
			auto scene = SceneThree::createScene();
			Director::getInstance()->replaceScene(TransitionFade::create(0.1, scene));
		}
	};
	keyboardListener->onKeyReleased = [=](EventKeyboard::KeyCode keyCode, Event* event)
	{
		// remove the key.  std::map.erase() doesn't care if the key doesnt exist
		keys.erase(keyCode);
	};

	//notify the event dispatcher of the new event
	this->_eventDispatcher->addEventListenerWithSceneGraphPriority(keyboardListener, this);

	//Read XML to build scene
	XMLTool::loadSceneFromXML(this, "../Resources/scene1.xml");
	


	//

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
										   CC_CALLBACK_1(SceneOne::menuCloseCallback, this));
    
	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));

    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

	// Let cocos know we have an update function to be called.
	this->scheduleUpdate();

    return true;
}

void SceneOne::update(float delta)
{
	Node::update(delta);
	if (isKeyPressed(EventKeyboard::KeyCode::KEY_RIGHT_ARROW)) 
	{
		//Update player position
		Sprite* player = dynamic_cast<Sprite*>(this->getChildByName("mario"));
		float positionX = player->getPositionX();
		player->setPositionX(positionX + 1.0f);

		//make the sprite face in the right direction
		if (player->isFlipX())
			player->setFlippedX(false);
	}

	if (isKeyPressed(EventKeyboard::KeyCode::KEY_LEFT_ARROW))
	{
		//Update player position
		Sprite* player = dynamic_cast<Sprite*>(this->getChildByName("mario"));
		float positionX = player->getPositionX();
		player->setPositionX(positionX - 1.0f);

		//make the sprite face in the right direction
		if (!player->isFlipX())
			player->setFlippedX(true);
	}

}

bool SceneOne::isKeyPressed(EventKeyboard::KeyCode code)
{
	// Check if the key is currently pressed by seeing it it's in the std::map keys
	// In retrospect, keys is a terrible name for a key/value paried datatype isnt it?
	if (keys.find(code) != keys.end())
		return true;
	return false;
}

double SceneOne::keyPressedDuration(EventKeyboard::KeyCode code)
{
	if (!isKeyPressed(EventKeyboard::KeyCode::KEY_CTRL))
		return 0;  // Not pressed, so no duration obviously

	// Return the amount of time that has elapsed between now and when the user
	// first started holding down the key in milliseconds
	// Obviously the start time is the value we hold in our std::map keys
	return std::chrono::duration_cast<std::chrono::milliseconds>
		(std::chrono::high_resolution_clock::now() - keys[code]).count();
}

void SceneOne::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

// Because cocos2d-x requres createScene to be static, we need to make other non-pointer members static
std::map<cocos2d::EventKeyboard::KeyCode, std::chrono::high_resolution_clock::time_point> SceneOne::keys;