#ifndef __LANGUAGE_TYPES_H__
#define __LANGUAGE_TYPES_H__



const enum eLanguage
{
	LANGUAGE_ENGLISH = 0,
	LANGUAGE_FRENCH,
	LANGUAGE_GERMAN,
	LANGUAGE_ITALIAN,
	LANGUAGE_NONE
};

extern eLanguage currentLanguage;

#endif // __XML_TOOL_H__
