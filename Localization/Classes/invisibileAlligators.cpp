#include "invisibileAlligators.h"
#include "mainMenu.h"

USING_NS_CC;

Scene* InvisibleAlligators::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = InvisibleAlligators::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool InvisibleAlligators::init()
{
	//////////////////////////////
	// 1. super init first
	if (!LayerColor::initWithColor(Color4B(255, 255, 255, 255)))
	{
		return false;
	}

	currentPage = 0;
	paused = false;

	//Setup keyboard
	auto keyboardListener = EventListenerKeyboard::create();

	keyboardListener->onKeyPressed = [=](EventKeyboard::KeyCode keyCode, Event* event)
	{
		// If a key already exists, do nothing as it will already have a time stamp
		// Otherwise, set's the timestamp to now
		if (keys.find(keyCode) == keys.end())
		{
			keys[keyCode] = std::chrono::high_resolution_clock::now();
		}

		//Switch Language
		if (keyCode == EventKeyboard::KeyCode::KEY_1)
		{
			//Set the current language
			currentLanguage = LANGUAGE_ENGLISH;

			//Delete the old text
			for (int i = 0; i <= this->maxPage + 1; i++)
			{
				this->removeChildByTag(i);
			}

			//load new text
			XMLTool::loadLanguagesFromXML(this, currentLanguage, "../Resources/InvisibleAlligators.xml", this->maxPage);
		}

		//Switch Language
		if (keyCode == EventKeyboard::KeyCode::KEY_2)
		{
			//Set the current language
			currentLanguage = LANGUAGE_FRENCH;

			//Delete the old text
			for (int i = 0; i <= this->maxPage + 1; i++)
			{
				this->removeChildByTag(i);
			}

			//load new text
			XMLTool::loadLanguagesFromXML(this, currentLanguage, "../Resources/InvisibleAlligators.xml", this->maxPage);
		}

		//Switch Language
		if (keyCode == EventKeyboard::KeyCode::KEY_3)
		{
			//Set the current language
			currentLanguage = LANGUAGE_GERMAN;

			//Delete the old text
			for (int i = 0; i <= this->maxPage + 1; i++)
			{
				this->removeChildByTag(i);
			}

			//load new text
			XMLTool::loadLanguagesFromXML(this, currentLanguage, "../Resources/InvisibleAlligators.xml", this->maxPage);
		}

		//Switch Language
		if (keyCode == EventKeyboard::KeyCode::KEY_4)
		{
			//Set the current language
			currentLanguage = LANGUAGE_ITALIAN;

			//Delete the old text
			for (int i = 0; i <= this->maxPage + 1; i++)
			{
				this->removeChildByTag(i);
			}

			//load new text
			XMLTool::loadLanguagesFromXML(this, currentLanguage, "../Resources/InvisibleAlligators.xml", this->maxPage);
		}

		//Pause
		if (keyCode == EventKeyboard::KeyCode::KEY_P)
		{
			paused = !paused;
		}

		//return home
		if (keyCode == EventKeyboard::KeyCode::KEY_SPACE)
		{
			auto scene = MainMenu::createScene();
			Director::getInstance()->replaceScene(TransitionJumpZoom::create(0.5, scene));
		}
	};

	keyboardListener->onKeyReleased = [=](EventKeyboard::KeyCode keyCode, Event* event)
	{
		// remove the key.  std::map.erase() doesn't care if the key doesnt exist
		keys.erase(keyCode);
	};

	//notify the event dispatcher of the new event
	this->_eventDispatcher->addEventListenerWithSceneGraphPriority(keyboardListener, this);

	//Load in the langauge via XML
	XMLTool::loadLanguagesFromXML(this, currentLanguage, "../Resources/InvisibleAlligators.xml", this->maxPage);

	//Load the images via XML
	XMLTool::loadImagesFromXML(this, "../Resources/invisgatorsimages.xml");


	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	// add "HelloWorld" splash screen"
	auto background = Sprite::create("../resources/pauseSquare.png");

	// position the sprite on the center of the screen
	background->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2));
	background->setTag(999);

	// add the sprite as a child to this layer
	this->addChild(background, 2);

	// add a "close" icon to exit the progress. it's an autorelease object
	auto nextPage = MenuItemImage::create(
		"../Resources/nextArrow.png",
		"../Resources/nextArrowClicked.png",
		CC_CALLBACK_1(InvisibleAlligators::nextPageCallback, this));

	nextPage->setPosition(Vec2(visibleSize.width * 0.5 + nextPage->getContentSize().width, visibleSize.height * 0.05));

	// add a "close" icon to exit the progress. it's an autorelease object
	auto lastPage = MenuItemImage::create(
		"../Resources/lastArrow.png",
		"../Resources/lastArrowClicked.png",
		CC_CALLBACK_1(InvisibleAlligators::previousPageCallback, this));

	lastPage->setPosition(Vec2(visibleSize.width * 0.5 - nextPage->getContentSize().width, visibleSize.height * 0.05));

	// create menu, it's an autorelease object
	auto menu = Menu::create(nextPage, lastPage, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, 1);

	//Schedule the update call
	this->scheduleUpdate();

	return true;
}

//Call the Cocos2d update
void InvisibleAlligators::update(float dt)
{
	Node::update(dt);

	//Display the text for the current page
	this->getChildByTag(currentPage)->setVisible(true);

	//Display the image for the current page
	this->getChildByTag(currentPage + 1000)->setVisible(true);

	this->getChildByTag(999)->setVisible(paused);
	this->getChildByTag(this->maxPage + 1)->setVisible(paused);
}


//Turn the page
void InvisibleAlligators::nextPageCallback(cocos2d::Ref* pSender)
{
	this->getChildByTag(currentPage)->setVisible(false);
	this->getChildByTag(currentPage + 1000)->setVisible(false);

	if (this->currentPage < this->maxPage)
		this->currentPage++;
}

//Turn to previous page
void InvisibleAlligators::previousPageCallback(cocos2d::Ref* pSender)
{
	this->getChildByTag(currentPage)->setVisible(false);
	this->getChildByTag(currentPage + 1000)->setVisible(false);

	if (this->currentPage >= 1)
		this->currentPage--;
}

void InvisibleAlligators::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.", "Alert");
	return;
#endif

	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}

bool InvisibleAlligators::isKeyPressed(EventKeyboard::KeyCode code)
{
	// Check if the key is currently pressed by seeing it it's in the std::map keys
	// In retrospect, keys is a terrible name for a key/value paried datatype isnt it?
	if (keys.find(code) != keys.end())
		return true;
	return false;
}

double InvisibleAlligators::keyPressedDuration(EventKeyboard::KeyCode code)
{
	if (!isKeyPressed(EventKeyboard::KeyCode::KEY_CTRL))
		return 0;  // Not pressed, so no duration obviously

	// Return the amount of time that has elapsed between now and when the user
	// first started holding down the key in milliseconds
	// Obviously the start time is the value we hold in our std::map keys
	return std::chrono::duration_cast<std::chrono::milliseconds>
		(std::chrono::high_resolution_clock::now() - keys[code]).count();
}

// Because cocos2d-x requres createScene to be static, we need to make other non-pointer members static
std::map<cocos2d::EventKeyboard::KeyCode, std::chrono::high_resolution_clock::time_point> InvisibleAlligators::keys;