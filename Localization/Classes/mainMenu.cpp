#include "mainMenu.h"
#include "pirateMonkey.h"
#include "invisibileAlligators.h"
#include "dogMagnet.h"
#include "XMLTool.h"

USING_NS_CC;

Scene* MainMenu::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = MainMenu::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool MainMenu::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	cocos2d::Size visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
	cocos2d::Vec2 origin = cocos2d::Director::getInstance()->getVisibleOrigin();

	//Create title label
	auto title = cocos2d::Label::createWithTTF("Children's Stories", "../resources/fonts/impact.ttf", 60);
	title->setTag(1);

	// position the label on the center of the screen
	title->setPosition(cocos2d::Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height * 0.42));

	// add the label as a child to this layer
	this->addChild(title, 1);

	//Create label
	auto storyOne = cocos2d::Label::createWithTTF("The Brave Monkey Pirate", "../resources/fonts/impact.ttf", 26);
	auto openStoryOne = cocos2d::MenuItemLabel::create(storyOne, CC_CALLBACK_1(MainMenu::loadPirateMonkey, this));
	openStoryOne->setTag(1);

	//Set position of menu item
	openStoryOne->setPosition(cocos2d::Vec2(origin.x + visibleSize.width * 0.2, origin.y + visibleSize.height * 0.30));

	//Create label
	auto storyTwo = cocos2d::Label::createWithTTF("Invisibile Alligators", "../resources/fonts/impact.ttf", 26);
	auto openStoryTwo = cocos2d::MenuItemLabel::create(storyTwo, CC_CALLBACK_1(MainMenu::loadInvisibleAlligators, this));
	openStoryTwo->setTag(2);
	openStoryTwo->setPosition(cocos2d::Vec2(origin.x + visibleSize.width * 0.5, origin.y + visibleSize.height * 0.30));

	//Create label
	auto storyThree = cocos2d::Label::createWithTTF("The Wiener Dog Magnet", "../resources/fonts/impact.ttf", 26);
	auto openStoryThree = cocos2d::MenuItemLabel::create(storyThree, CC_CALLBACK_1(MainMenu::loadWienerDog, this));
	openStoryThree->setTag(3);

	//Set position of menu item
	openStoryThree->setPosition(cocos2d::Vec2(origin.x + visibleSize.width * 0.80, origin.y + visibleSize.height * 0.30));

	auto menu = cocos2d::Menu::create(openStoryOne, openStoryTwo, openStoryThree, NULL);
	menu->setPosition(cocos2d::Vec2::ZERO);
	this->addChild(menu, 1);
	menu->setTag(2);

	//Create title label
	auto languages = cocos2d::Label::createWithTTF("1:English  2:French  3:German  4:Italian", "../resources/fonts/impact.ttf", 28);
	languages->setTag(3);

	// position the label on the center of the screen
	languages->setPosition(cocos2d::Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height * 0.15));

	// add the label as a child to this layer
	this->addChild(languages, 1);

	// add "HelloWorld" splash screen"
	auto background = Sprite::create("../resources/menu.png");

	// position the sprite on the center of the screen
	background->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2));

	// add the sprite as a child to this layer
	this->addChild(background, 0);


	//Load the current menu
	XMLTool::loadMainMenu(this, currentLanguage, "../Resources/menu.xml");

	//Setup keyboard
	auto keyboardListener = EventListenerKeyboard::create();

	keyboardListener->onKeyPressed = [=](EventKeyboard::KeyCode keyCode, Event* event)
	{
		// If a key already exists, do nothing as it will already have a time stamp
		// Otherwise, set's the timestamp to now
		if (keys.find(keyCode) == keys.end())
		{
			keys[keyCode] = std::chrono::high_resolution_clock::now();
		}

		//Switch Language
		if (keyCode == EventKeyboard::KeyCode::KEY_1)
		{
			//Set the current language
			currentLanguage = LANGUAGE_ENGLISH;
			//Load the current menu
			XMLTool::loadMainMenu(this, currentLanguage, "../Resources/menu.xml");
		}

		//Switch Language
		if (keyCode == EventKeyboard::KeyCode::KEY_2)
		{
			//Set the current language
			currentLanguage = LANGUAGE_FRENCH;

			//Load the current menu
			XMLTool::loadMainMenu(this, currentLanguage, "../Resources/menu.xml");
		}

		//Switch Language
		if (keyCode == EventKeyboard::KeyCode::KEY_3)
		{
			//Set the current language
			currentLanguage = LANGUAGE_GERMAN;

			//Load the current menu
			XMLTool::loadMainMenu(this, currentLanguage, "../Resources/menu.xml");
		}

		//Switch Language
		if (keyCode == EventKeyboard::KeyCode::KEY_4)
		{
			//Set the current language
			currentLanguage = LANGUAGE_ITALIAN;

			//Load the current menu
			XMLTool::loadMainMenu(this, currentLanguage, "../Resources/menu.xml");
		}
	};

	keyboardListener->onKeyReleased = [=](EventKeyboard::KeyCode keyCode, Event* event)
	{
		// remove the key.  std::map.erase() doesn't care if the key doesnt exist
		keys.erase(keyCode);
	};

	//notify the event dispatcher of the new event
	this->_eventDispatcher->addEventListenerWithSceneGraphPriority(keyboardListener, this);

	return true;
}

//load scene on callback
void MainMenu::loadPirateMonkey(Ref* pSender)
{
	auto scene = PirateMonkey::createScene();
	Director::getInstance()->replaceScene(TransitionJumpZoom::create(0.5, scene));
}

//load scene on callback
void MainMenu::loadWienerDog(Ref* pSender)
{
	auto scene = dogMagnet::createScene();
	Director::getInstance()->replaceScene(TransitionJumpZoom::create(0.5, scene));
}

//load scene on callback
void MainMenu::loadInvisibleAlligators(Ref* pSender)
{
	auto scene = InvisibleAlligators::createScene();
	Director::getInstance()->replaceScene(TransitionJumpZoom::create(0.5, scene));
}

void MainMenu::setMenu(Layer* Layer)
{
	//XMLTool::loadMainMenu(Layer, "");

	//Update title text
	Label* label = (Label*)Layer->getChildByTag(1);
	label->setString("TEST1");

	//Get the menu
	Menu* storyOne = (Menu*)Layer->getChildByTag(2);

	//Update the text for the menus
	MenuItemLabel* item = (MenuItemLabel*)storyOne->getChildByTag(1);
	label = (Label*)item->getLabel();
	label->setString("TEST2");

	item = (MenuItemLabel*)storyOne->getChildByTag(2);
	label = (Label*)item->getLabel();
	label->setString("TEST3");

	item = (MenuItemLabel*)storyOne->getChildByTag(3);
	label = (Label*)item->getLabel();
	label->setString("TEST4");

	//Update languages
	label = (Label*)Layer->getChildByTag(3);
	label->setString("TEST5");

}

void MainMenu::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.", "Alert");
	return;
#endif

	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}

bool MainMenu::isKeyPressed(EventKeyboard::KeyCode code)
{
	// Check if the key is currently pressed by seeing it it's in the std::map keys
	// In retrospect, keys is a terrible name for a key/value paried datatype isnt it?
	if (keys.find(code) != keys.end())
		return true;
	return false;
}

double MainMenu::keyPressedDuration(EventKeyboard::KeyCode code)
{
	if (!isKeyPressed(EventKeyboard::KeyCode::KEY_CTRL))
		return 0;  // Not pressed, so no duration obviously

	// Return the amount of time that has elapsed between now and when the user
	// first started holding down the key in milliseconds
	// Obviously the start time is the value we hold in our std::map keys
	return std::chrono::duration_cast<std::chrono::milliseconds>
		(std::chrono::high_resolution_clock::now() - keys[code]).count();
}

// Because cocos2d-x requres createScene to be static, we need to make other non-pointer members static
std::map<cocos2d::EventKeyboard::KeyCode, std::chrono::high_resolution_clock::time_point> MainMenu::keys;