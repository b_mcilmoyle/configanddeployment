#ifndef __INVISIBLE_ALLIGATORS_H__
#define __INVISIBLE_ALLIGATORS_H__

#include "cocos2d.h"
#include "XMLTool.h"

class InvisibleAlligators : public cocos2d::LayerColor
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
	void nextPageCallback(cocos2d::Ref* pSender);
	void previousPageCallback(cocos2d::Ref* pSender);

	//Call the virtual update to update each frame
	virtual void update(float);

    // implement the "static create()" method manually
	CREATE_FUNC(InvisibleAlligators);

	bool isKeyPressed(cocos2d::EventKeyboard::KeyCode);
	double keyPressedDuration(cocos2d::EventKeyboard::KeyCode);

private:

	int currentPage;
	int maxPage;
	bool paused;

	static std::map<cocos2d::EventKeyboard::KeyCode, std::chrono::high_resolution_clock::time_point> keys;

};

#endif // __INVISIBLE_ALLIGATORS_H__
