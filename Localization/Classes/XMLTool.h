#ifndef __XML_TOOL_H__
#define __XML_TOOL_H__

#include "cocos2d.h"
#include <map>
#include <vector>
#include <string>
#include "Global.h"

class XMLTool
{
public:

	static void loadLanguagesFromXML(cocos2d::Layer* Layer, eLanguage language, std::string fileName, int& maxPage);
	static void loadImagesFromXML(cocos2d::Layer* Layer, std::string fileName);
	static void loadMainMenu(cocos2d::Layer* Layer, eLanguage language, std::string fileName);

private:
};

#endif // __XML_TOOL_H__
