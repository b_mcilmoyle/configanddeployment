#include "XMLTool.h"
#include "rapidxml_utils.hpp"
#include "rapidxml_print.hpp"
#include "mainMenu.h"


void XMLTool::loadLanguagesFromXML(cocos2d::Layer* Layer, eLanguage language, std::string xmlFileName, int& maxPage)
{
	//Open the XML file
	rapidxml::file<> xmlFile(xmlFileName.c_str()); // Default template is char
	rapidxml::xml_document<> doc;
	doc.parse<0>(xmlFile.data());

	//Set the root node
	rapidxml::xml_node<>* root = doc.first_node("Localization");

	//Set the first child "sprite" node
	rapidxml::xml_node<>* languageNode = root->first_node("Language");

	//Loop through each language
	while (languageNode != NULL)
	{
		//Get the which language the node represents
		std::string languageID = languageNode->first_attribute("LanguageID")->value();

		//Only read the lanague we need
		if (language == eLanguage::LANGUAGE_ENGLISH)
		{
			if (languageID != "EN")
			{
				languageNode = languageNode->next_sibling("Language");
				continue;
			}
		}
		else if (language == eLanguage::LANGUAGE_FRENCH)
		{
			if (languageID != "FR")
			{
				languageNode = languageNode->next_sibling("Language");
				continue;
			}
		}
		else if (language == eLanguage::LANGUAGE_GERMAN)
		{
			if (languageID != "GM")
			{
				languageNode = languageNode->next_sibling("Language");
				continue;
			}
		}
		else if (language == eLanguage::LANGUAGE_ITALIAN)
		{
			if (languageID != "IT")
			{
				languageNode = languageNode->next_sibling("Language");
				continue;
			}
		}

		//Load the strings to label
		rapidxml::xml_node<>* stringNode = languageNode->first_node("String");

		int stringCount = 0;

		while (stringNode != NULL)
		{
			//get the sentance
			std::string sentance = stringNode->value();

			//Create title label
			auto label = cocos2d::Label::createWithTTF(sentance, "../resources/fonts/impact.ttf", 26);

			//Make the text black
			label->setColor(cocos2d::Color3B(0, 0, 0));

			//Get screen size
			cocos2d::Size visibleSize = cocos2d::Director::getInstance()->getVisibleSize();

			// position the label on the center of the screen
			label->setPosition(cocos2d::Vec2(visibleSize.width * 0.5, visibleSize.height * 0.30));

			//Add an ID that was can use to call this label later
			label->setTag(stringCount);

			label->setVisible(false);

			// add the label as a child to this layer
			Layer->addChild(label, 1);

			//Find the next language node
			stringNode = stringNode->next_sibling("String");

			//increase count
			stringCount++;
		}

		//Set how many pages are in the story
		stringCount--;
		maxPage = stringCount;

		

		//Set pause menu
		//get the sentance
		rapidxml::xml_node<>* pauseNode = languageNode->first_node("pause");

		//Create title label
		auto pauseLabel = cocos2d::Label::createWithTTF(pauseNode->value(), "../resources/fonts/impact.ttf", 32);

		//Get screen size
		cocos2d::Size visibleSize = cocos2d::Director::getInstance()->getVisibleSize();

		// position the label on the center of the screen
		pauseLabel->setPosition(cocos2d::Vec2(visibleSize.width * 0.5, visibleSize.height * 0.5));

		//Add an ID that was can use to call this label later
		pauseLabel->setTag(stringCount + 1);

		//label->setVisible(false);

		// add the label as a child to this layer
		Layer->addChild(pauseLabel, 5);

		//Find the next language node
		languageNode = languageNode->next_sibling("pause");
	}
}

void XMLTool::loadImagesFromXML(cocos2d::Layer* Layer, std::string xmlFileName)
{
	//Open the XML file
	rapidxml::file<> xmlFile(xmlFileName.c_str()); // Default template is char
	rapidxml::xml_document<> doc;
	doc.parse<0>(xmlFile.data());

	//Set the root node
	rapidxml::xml_node<>* root = doc.first_node("scene");

	//Set the first child "sprite" node
	rapidxml::xml_node<>* spriteNode = root->first_node("sprite");

	int imageCount = 0;

	//Loop through each language
	while (spriteNode != NULL)
	{

		//Load the strings to label
		rapidxml::xml_node<>* ImageNode = spriteNode->first_node("image");

		while (ImageNode != NULL)
		{
			//get the sentance
			std::string sentance = ImageNode->value();

			//Create title label
			auto image = cocos2d::Sprite::create(ImageNode->value());

			//Get screen size
			cocos2d::Size visibleSize = cocos2d::Director::getInstance()->getVisibleSize();

			// position the label on the center of the screen
			image->setPosition(cocos2d::Vec2(visibleSize.width * 0.5, visibleSize.height * 0.4 + image->getContentSize().height / 2));

			//Add an ID that was can use to call this label later. Use an offset to ID the images different then labels
			image->setTag(1000 + imageCount);

			image->setVisible(false);

			// add the label as a child to this layer
			Layer->addChild(image, 1);

			//Find the next language node
			ImageNode = ImageNode->next_sibling("image");
		}

		//increase count
		imageCount++;

		//Find the next sprite
		spriteNode = spriteNode->next_sibling("sprite");
	}
}

void XMLTool::loadMainMenu(cocos2d::Layer* Layer, eLanguage language, std::string xmlFileName)
{
	//Open the XML file
	rapidxml::file<> xmlFile(xmlFileName.c_str()); // Default template is char
	rapidxml::xml_document<> doc;
	doc.parse<0>(xmlFile.data());

	//Set the root node
	rapidxml::xml_node<>* root = doc.first_node("Localization");

	//Set the first child "sprite" node
	rapidxml::xml_node<>* languageNode = root->first_node("Language");

	//Loop through each language
	while (languageNode != NULL)
	{
		//Get the which language the node represents
		std::string languageID = languageNode->first_attribute("LanguageID")->value();

		//Only read the lanague we need
		if (language == eLanguage::LANGUAGE_ENGLISH)
		{
			if (languageID != "EN")
			{
				languageNode = languageNode->next_sibling("Language");
				continue;
			}
		}
		else if (language == eLanguage::LANGUAGE_FRENCH)
		{
			if (languageID != "FR")
			{
				languageNode = languageNode->next_sibling("Language");
				continue;
			}
		}
		else if (language == eLanguage::LANGUAGE_GERMAN)
		{
			if (languageID != "GM")
			{
				languageNode = languageNode->next_sibling("Language");
				continue;
			}
		}
		else if (language == eLanguage::LANGUAGE_ITALIAN)
		{
			if (languageID != "IT")
			{
				languageNode = languageNode->next_sibling("Language");
				continue;
			}
		}

		//Load the strings to label
		rapidxml::xml_node<>* stringNode = languageNode->first_node("String");

		//Update title text
		cocos2d::Label* label = (cocos2d::Label*)Layer->getChildByTag(1);
		label->setString(stringNode->value());

		//Find the next language node
		stringNode = stringNode->next_sibling("String");

		//Get the menu
		cocos2d::Menu* storyOne = (cocos2d::Menu*)Layer->getChildByTag(2);

		//Update the text for the menus
		cocos2d::MenuItemLabel* item = (cocos2d::MenuItemLabel*)storyOne->getChildByTag(1);
		label = (cocos2d::Label*)item->getLabel();
		label->setString(stringNode->value());

		//Find the next language node
		stringNode = stringNode->next_sibling("String");

		item = (cocos2d::MenuItemLabel*)storyOne->getChildByTag(2);
		label = (cocos2d::Label*)item->getLabel();
		label->setString(stringNode->value());

		//Find the next language node
		stringNode = stringNode->next_sibling("String");

		item = (cocos2d::MenuItemLabel*)storyOne->getChildByTag(3);
		label = (cocos2d::Label*)item->getLabel();
		label->setString(stringNode->value());

		//Find the next language node
		stringNode = stringNode->next_sibling("String");

		label = (cocos2d::Label*)Layer->getChildByTag(3);
		label->setString(stringNode->value());

		//Find the next language node
		languageNode = languageNode->next_sibling("Language");
	}
}