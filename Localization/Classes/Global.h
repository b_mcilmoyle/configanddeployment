#ifndef __GLOBAL_H__
#define __GLOBAL_H__

const enum eLanguage
{
	LANGUAGE_ENGLISH = 0,
	LANGUAGE_FRENCH,
	LANGUAGE_GERMAN,
	LANGUAGE_ITALIAN,
	LANGUAGE_NONE
};

extern eLanguage currentLanguage;

#endif // __GLOBAL_H__
